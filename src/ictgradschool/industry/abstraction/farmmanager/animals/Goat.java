package ictgradschool.industry.abstraction.farmmanager.animals;


public class Goat extends Animal {
    private final int MAX_VALUE = 1000;

    public Goat() {
        value = 800;
    }

    public void feed() {
        if (value < MAX_VALUE) {
            value += 70;
        }
    }

    public int costToFeed() {
        return 40;
    }

    public String getType() {
        return "Goat";
    }

    public String toString() {
        return getType() + " - $" + value;
    }
}